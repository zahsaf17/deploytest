import express from 'express'
// Create a new express app instance
const app: express.Application = express();
const port: string = process.env.port || '3000'
app.set('view engine', 'pug')
app.get('/', (req, res) => {
    res.render('index')
});
app.listen(port)