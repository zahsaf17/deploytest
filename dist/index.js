"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
// Create a new express app instance
const app = express_1.default();
const port = process.env.port || '3000';
app.set('view engine', 'pug');
app.get('/', (req, res) => {
    res.render('index');
});
app.listen(port);
//# sourceMappingURL=index.js.map