const path = require('path');
const nodeExternals = require('webpack-node-externals');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './dist/index.js',
  plugins: [
    new CopyPlugin({
        patterns: [
            { from: __dirname + '/views', to: __dirname + '/app/views'},
            { from: __dirname + '/web.config', to: __dirname + '/app/web.config'},
        ]
    }),
  ],
  target: 'node',
  externals: [nodeExternals()],
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'app/'),
  },
};